package logfilter

// GOALS:
// LOG="debug"
// LOG="DEBUG"
// LOG="DEBUG:package/*"
// LOG="DEBUG:package/*,cli/*"
// LOG="DEBUG:package/*,-http/*"

// LOG=connect/bodyparser,connect/compress,connect/session,redis/*,-http
// LOG=DEBUG:package/*,-package/di
