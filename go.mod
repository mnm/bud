module github.com/livebud/bud

go 1.18

require (
	github.com/Bowery/prompt v0.0.0-20190916142128-fa8279994f75
	github.com/PuerkitoBio/goquery v1.8.0
	github.com/ajg/form v1.5.2-0.20200323032839-9aeb3cf462e1
	github.com/armon/go-radix v1.0.0
	github.com/cespare/xxhash v1.1.0
	github.com/evanw/esbuild v0.14.11
	github.com/fatih/structtag v1.2.0
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gitchander/permutation v0.0.0-20201214100618-1f3e7285f953
	github.com/hexops/valast v1.4.1
	github.com/lithammer/dedent v1.1.0
	github.com/matryer/is v1.4.0
	github.com/matthewmueller/diff v0.0.0-20220104030700-cb2fe910d90c
	github.com/matthewmueller/gotext v0.0.0-20210424201144-265ed61725ac
	github.com/matthewmueller/text v0.0.0-20210424201111-ec1e4af8dfe8
	github.com/mattn/go-isatty v0.0.14
	github.com/monochromegane/go-gitignore v0.0.0-20200626010858-205db1a8cc00
	github.com/otiai10/copy v1.7.0
	github.com/timewasted/go-accept-headers v0.0.0-20130320203746-c78f304b1b09
	github.com/xlab/treeprint v1.1.0
	go.kuoruan.net/v8go-polyfills v0.5.0
	go.opentelemetry.io/otel v1.4.1
	go.opentelemetry.io/otel/sdk v1.4.1
	go.opentelemetry.io/otel/trace v1.4.1
	golang.org/x/mod v0.5.1
	golang.org/x/sync v0.0.0-20210220032951-036812b2e83c
	golang.org/x/tools v0.1.9
	rogchap.com/v8go v0.7.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/gedex/inflector v0.0.0-20170307190818-16278e9db813 // indirect
	github.com/go-logr/logr v1.2.2 // indirect
	github.com/go-logr/stdr v1.2.2 // indirect
	github.com/google/go-cmp v0.5.7 // indirect
	github.com/kr/pretty v0.3.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/rogpeppe/go-internal v1.8.1 // indirect
	github.com/sergi/go-diff v1.2.0 // indirect
	github.com/shurcooL/go-goon v0.0.0-20170922171312-37c2f522c041 // indirect
	golang.org/x/net v0.0.0-20211015210444-4f30a5c0130f // indirect
	golang.org/x/sys v0.0.0-20220408201424-a24fb2fb8a0f // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	mvdan.cc/gofumpt v0.2.0 // indirect
)
